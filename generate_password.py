import random
import string

def password_generator(special_characters, numbers, min_length, max_length):
    password = ""
    characters = string.ascii_letters + string.digits + string.punctuation
    length = random.randint(min_length, max_length)
    if special_characters and numbers:
        while special_characters or numbers:
            special_characters = True
            numbers = True
            password = ''.join(random.choice(characters) for i in range(length))
            if any(char.isdigit() for char in password):
                numbers = False
            if any(not char.isalnum() for char in password):
                special_characters = False
    elif numbers:
        while numbers:
            password = ''.join(random.choice(characters) for i in range(length))
            if any(char.isdigit() for char in password):
                numbers = False
    elif special_characters:
        while special_characters:
            password = ''.join(random.choice(characters) for i in range(length))
            if any(not char.isalnum() for char in password):
                special_characters = False
    return password
