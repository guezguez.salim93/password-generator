import generate_password


class Password:
    def __init__(self, website, special_characters, numbers, min_length=10, max_length=20):
        self.website = website
        self.special_characters = special_characters
        self.numbers = numbers
        self.max_length = max_length
        self.min_length = min_length
        self._password = generate_password.password_generator(self.special_characters, self.numbers, self.min_length, self.max_length)

    @property
    def get_password(self):
        return self._password

    @property
    def get_website(self):
        return self.website

    @property
    def get_website_and_password(self):
        return f"your Password for {self.website} is {self.get_password}"
