from Password import Password
import tkinter as tk

last_password = []


def button_click_generate():
    website_payload = website.get()
    min_length_payload = min_length.get()
    max_length_payload = max_length.get()
    special_characters = var1.get()
    numbers = var2.get()
    pswd = Password(website_payload, bool(special_characters), bool(numbers), int(min_length_payload),
                    int(max_length_payload))
    last_password.append(pswd.get_password)
    password.config(text=pswd.get_password)


def button_click_save():
    print(last_password)
    with open('passwords.txt', 'a') as f:
        f.write('\n')
        f.write(f"your {website.get()} password is " + last_password[len(last_password) - 1])


if __name__ == '__main__':
    # main()
    window = tk.Tk()
    greeting = tk.Label(text="Welcome to password generator")
    greeting.pack()
    label_website = tk.Label(text="For which website do you need a password ?")
    website = tk.Entry()
    label_website.pack()
    website.pack()
    var1 = tk.IntVar()
    var2 = tk.IntVar()
    c1 = tk.Checkbutton(window, text='special characters', variable=var1, onvalue=1, offvalue=0)
    c1.pack()
    c2 = tk.Checkbutton(window, text='numbers', variable=var2, onvalue=1, offvalue=0)
    c2.pack()

    label_min_length = tk.Label(text="min length :")
    min_length = tk.Entry()
    label_min_length.pack()
    min_length.pack()

    label_max_length = tk.Label(text="max length :")
    max_length = tk.Entry()
    label_max_length.pack()
    max_length.pack()

    generate = tk.Button(text="Generate Password", command=button_click_generate)
    generate.pack()

    label_password = tk.Label(window, text="Password = ")
    label_password.pack()
    password = tk.Label(window, bg='gray', text='')
    password.pack()

    label_save_password = tk.Button(text="Save Password to txt file", command=button_click_save)
    label_save_password.pack()
    window.mainloop()
